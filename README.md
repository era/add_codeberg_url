# Migrating to Codeberg

I have a bunch of repositories that were originally created on Github, and I would like to push any modification I make to both Github and Codeberg.

I have the same user name on both platforms and the repository names are always the same.

So I created this small script that given a git repo already pointing remote to github, adds the codeberg url, so when I do `git push` it push to both remotes.

